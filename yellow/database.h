#pragma once

#ifndef HEADERFILE_DATABASE
#define HEADERFILE_DATABASE

#include <algorithm>
#include <functional>
#include <map>
#include <string>
#include <vector>
#include "date.h"

using namespace std;

class Database
{
public:
    Database();
    void Add(const Date& date, const string& event) noexcept;
    bool Del(const Date& date, const string& event) noexcept;
    size_t Del(const Date& date) noexcept;
    vector<string>& Find(const Date& date) noexcept;
    void Print(ostream& os) const noexcept;
    vector<string> FindIf(const std::function<bool(const Date&, const string&)>& predicate) const noexcept;
    size_t RemoveIf(const std::function<bool(const Date&, const string&)>& predicate) noexcept;
    string Last(const Date& date) const;
    bool IsEventExistsForDate(const Date& date, const string& event) const noexcept;
private:
    map<Date, vector<string>> _events_by_date;
};
//----------------------------------------------------------------------------------------------------

#endif