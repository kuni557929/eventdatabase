#pragma once

#ifndef HEADERFILE_DATE
#define HEADERFILE_DATE

#include <string>
#include <stdexcept>
#include <sstream>
#include <iomanip>
#include <memory>

using namespace std;

class Date
{
public:
    Date() = default;
    Date(int year, int month, int day); 
    friend bool operator == (const Date& first, const Date& second);
    friend bool operator != (const Date& first, const Date& second);
    friend bool operator >  (const Date& first, const Date& second);
    friend bool operator >= (const Date& first, const Date& second);
    friend bool operator <  (const Date& first, const Date& second);
    friend bool operator <= (const Date& first, const Date& second);
    friend istream& operator >> (istream& is, Date& date);
    friend ostream& operator << (ostream& is, const Date& date);
    int GetYear() const noexcept;
    int GetMonth() const noexcept;
    int GetDay() const noexcept;
    string ToString() const noexcept;
private:
    int _year;
    int _month;
    int _day;
private:
    string Validate(int year, int month, int day) const noexcept;
    int DaysInMonth(int month, int year) const noexcept;
    bool IsLeapYear(int year) const noexcept;
};
//----------------------------------------------------------------------------------------------------

static Date ParseDate(istream& is)
{
    Date date;
    is >> date;
    return date;
};
//----------------------------------------------------------------------------------------------------

#endif