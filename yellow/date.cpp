#include "date.h"
#include <iostream>

Date::Date(int year, int month, int day)
{
    string message = Validate(year, month, day);

    if (!message.empty())
    {
        throw logic_error(message);
    }

    _year = year;
    _month = month;
    _day = day;
};
//----------------------------------------------------------------------------------------------------

string Date::Validate(int year, int month, int day) const noexcept
{
    if (month < 1 || month > 12)
    {
        return "Month value is invalid: " + to_string(month);
    }

    if (day < 1 || day > DaysInMonth(month, year))
    {
        return "Day value is invalid: " + to_string(day);
    }

    return "";
};
//----------------------------------------------------------------------------------------------------

int Date::DaysInMonth(int month, int year) const noexcept
{
    switch (month)
    {
        case 4:
        case 6:
        case 9:
        case 11:
            return 30;
        default:
            return 31;
    }
};
//----------------------------------------------------------------------------------------------------

bool Date::IsLeapYear(int year) const noexcept
{
    return year % 4 == 0 && (year % 100 != 0 || year % 400 == 0);
};
//----------------------------------------------------------------------------------------------------

int Date::GetYear() const noexcept
{
    return _year;
};
//----------------------------------------------------------------------------------------------------

int Date::GetMonth() const noexcept
{
    return _month;
};
//----------------------------------------------------------------------------------------------------

int Date::GetDay() const noexcept
{
    return _day;
};
//----------------------------------------------------------------------------------------------------

bool operator == (const Date& first, const Date& second)
{
    return first.GetYear() == second.GetYear() &&
           first.GetMonth() == second.GetMonth() &&
           first.GetDay() == second.GetDay();
};
//----------------------------------------------------------------------------------------------------

bool operator != (const Date& first, const Date& second)
{
    return !(first == second);
};
//----------------------------------------------------------------------------------------------------

bool operator > (const Date& first, const Date& second)
{
    return first.GetYear() > second.GetYear() ||
           (first.GetYear() == second.GetYear() && first.GetMonth() > second.GetMonth()) ||
           (first.GetYear() == second.GetYear() && first.GetMonth() == second.GetMonth() && first.GetDay() > second.GetDay());
};
//----------------------------------------------------------------------------------------------------

bool operator >= (const Date& first, const Date& second)
{
    return first > second || first == second;
};
//----------------------------------------------------------------------------------------------------

bool operator <  (const Date& first, const Date& second)
{
    return first.GetYear() < second.GetYear() ||
           (first.GetYear() == second.GetYear() && first.GetMonth() < second.GetMonth()) ||
           (first.GetYear() == second.GetYear() && first.GetMonth() == second.GetMonth() && first.GetDay() < second.GetDay());
};
//----------------------------------------------------------------------------------------------------

bool operator <= (const Date& first, const Date& second)
{
    return first < second || first == second;
};
//----------------------------------------------------------------------------------------------------

istream& operator >> (istream& is, Date& date)
{
    string string_date;
    is >> string_date;

    stringstream str_stream_date(string_date);

    int year, month, day;
    char sep1, sep2;

    if (str_stream_date >> year >> sep1 >> month >> sep2 >> day && sep1 == '-' && sep2 == '-')
    {
        date = Date(year, month, day);
    }
    else
    {
        throw logic_error("Wrong date format: " + string_date);
    }

    return is;
};
//----------------------------------------------------------------------------------------------------

string Date::ToString() const noexcept
{
    stringstream dateStream;
    dateStream << std::setfill('0') << std::setw(4) << this->GetYear()  << '-';
    dateStream << std::setfill('0') << std::setw(2) << this->GetMonth() << '-';
    dateStream << std::setfill('0') << std::setw(2) << this->GetDay();
    return dateStream.str();
};
//----------------------------------------------------------------------------------------------------

ostream& operator << (ostream& os, const Date& date)
{
    return os << date.ToString();
};
//----------------------------------------------------------------------------------------------------