#pragma once

#ifndef HEADERFILE_NODE
#define HEADERFILE_NODE

#include <string>
#include "date.h"

enum class Comparison
{
    Less,
    LessOrEqual,
    Greater,
    GreaterOrEqual,
    Equal,
    NotEqual
};
//----------------------------------------------------------------------------------------------------

enum class LogicalOperation
{
    And,
    Or
};
//----------------------------------------------------------------------------------------------------

class Node
{
public:
    virtual bool Evaluate(const Date& date, const string& event) const noexcept = 0;
    virtual ~Node() = default;
};
//----------------------------------------------------------------------------------------------------

class EmptyNode : public Node
{
public:
    EmptyNode();
    bool Evaluate(const Date& date, const string& event) const noexcept override;
};
//----------------------------------------------------------------------------------------------------

class DateComparisonNode : public Node
{
public:
    DateComparisonNode(const Comparison& comparison, const Date& date);
    bool Evaluate(const Date& date, const string& event) const noexcept override;
private:
    const Comparison comparison;
    const Date date;
};
//----------------------------------------------------------------------------------------------------

class EventComparisonNode : public Node
{
public:
    EventComparisonNode(const Comparison& comparison, const string& event);
    bool Evaluate(const Date& date, const string& event) const noexcept override;
private:
    const Comparison comparison;
    const string event;
};
//----------------------------------------------------------------------------------------------------

class LogicalOperationNode : public Node
{
public:
    LogicalOperationNode(const LogicalOperation& logical_operation, const shared_ptr<Node>& left, const shared_ptr<Node>& right);
    bool Evaluate(const Date& date, const string& event) const noexcept override;
private:
    const LogicalOperation logical_operation;
    const shared_ptr<Node> left;
    const shared_ptr<Node> right;
};
//----------------------------------------------------------------------------------------------------

#endif