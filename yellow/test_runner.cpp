#include "test_runner.h"

void TestRunner::RunTest(lambda func, const string& name)
{
    try
    {
        func();
        cerr << "PASSED: " + name << endl;
    }
    catch (const runtime_error& ex)
    {
        cerr << "FAIL: " << name << ex.what() << endl;
        fail_count++;
    }
};
//----------------------------------------------------------------------------------------------------

TestRunner::~TestRunner()
{
    if (fail_count > 0)
    {
        std::cerr << fail_count << " unit tests failed. Terminate" << std::endl;
        exit(1);
    }
};
//----------------------------------------------------------------------------------------------------