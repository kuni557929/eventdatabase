#pragma once

#ifndef HEADER_MAIN_TEST
#define HEADER_MAIN_TEST

class AlwaysFalseNode : public Node
{
    [[nodiscard]] bool Evaluate(const Date&, const std::string& event) const noexcept override;
};
//----------------------------------------------------------------------------------------------------

void TestParseEvent();
void TestParseDate();
void TestEmptyNode();
void TestDateComparisonNode();
void TestEventComparisonNode();
void TestLogicalOperationNode();
void TestInsertionOrder();
void TestAdd();
void TestLast();
void TestRemove();
void TestFind();
void TestDatabase();

#endif
