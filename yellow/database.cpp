#include "database.h"

Database::Database() : _events_by_date(map<Date, vector<string>>())
{
    // empty
};
//----------------------------------------------------------------------------------------------------

void Database::Add(const Date& date, const string& event) noexcept
{
    auto [it, inserted] = _events_by_date.emplace(date, vector<string>{event});

    if (inserted)
    {
        it->second.reserve(1);
    }

    if (!IsEventExistsForDate(it->first, event))
    {
        it->second.emplace_back(event);
    }
};
//----------------------------------------------------------------------------------------------------

bool Database::Del(const Date& date, const string& event) noexcept
{
    if (_events_by_date.count(date) && IsEventExistsForDate(date, event))
    {
        vector<string>& events = _events_by_date.at(date);
        events.erase(remove(events.begin(), events.end(), event), events.end());

        if (events.empty())
        {
            _events_by_date.erase(date);
        }

        return true;
    }

    return false;
};
//----------------------------------------------------------------------------------------------------

size_t Database::Del(const Date& date) noexcept
{
    if (_events_by_date.count(date))
    {
        size_t deleted_events = _events_by_date.at(date).size();
        _events_by_date.erase(date);
        return deleted_events;
    }

    return 0;
};
//----------------------------------------------------------------------------------------------------

vector<string>& Database::Find(const Date& date) noexcept
{
    if (!_events_by_date.count(date))
    {
        static vector<string> empty;
        return empty;
    }

    return _events_by_date.at(date);
};
//----------------------------------------------------------------------------------------------------

bool Database::IsEventExistsForDate(const Date& date, const string& event) const noexcept
{
    if (_events_by_date.count(date))
    {
        const vector<string>& events = _events_by_date.at(date);
        return find(events.begin(), events.end(), event) != events.end();
    }

    return false;
};
//----------------------------------------------------------------------------------------------------

void Database::Print(ostream& cout) const noexcept
{
    for (const auto& [date, events] : _events_by_date)
    {
        for (const auto& event : events)
        {
            cout << date << " " << event << endl;
        }
    }
};
//----------------------------------------------------------------------------------------------------

vector<string> Database::FindIf(const std::function<bool(const Date&, const string&)>& predicate) const noexcept
{
    vector<string> entries;

    for (const auto& [date, events] : _events_by_date)
    {
        for (const string& event : events)
        {
            if (predicate(date, event))
            {
                entries.emplace_back(date.ToString() + " " + event);
            }
        }
    }

    return entries;
};
//----------------------------------------------------------------------------------------------------

string Database::Last(const Date& date) const
{
    const Date* largest_date = nullptr;
    const vector<string>* entries = nullptr;

    for (const auto& [first, events] : _events_by_date)
    {
        if (first <= date && (largest_date == nullptr || first > *largest_date))
        {
            largest_date = &first;
            entries = &events;
        }
    }

    if (entries != nullptr && !entries->empty())
    {
        return largest_date->ToString() + " " + entries->at(entries->size() - 1);
    }

    throw invalid_argument("No entries");
};
//----------------------------------------------------------------------------------------------------

size_t Database::RemoveIf(const std::function<bool(const Date&, const string&)>& predicate) noexcept
{
    size_t count_of_deleted = 0;

    for (auto& [date, events] : _events_by_date)
    {
        size_t initial_size = events.size();
        events.erase(std::remove_if(events.begin(), events.end(), [predicate, &date](const string& event) { return predicate(date, event); }), events.end());
        count_of_deleted += (initial_size - events.size());
    }

    for (auto it = _events_by_date.begin(); it != _events_by_date.end(); )
    {
        if (it->second.empty())
        {
            it = _events_by_date.erase(it);
        }
        else
        {
            ++it;
        }
    }

    return count_of_deleted;
};
//----------------------------------------------------------------------------------------------------