#pragma once

#ifndef HEADER_CONDITION_PARSE
#define HEADER_CONDITION_PARSE

#include "node.h"
#include "test_runner.h"

shared_ptr<Node> ParseCondition(istream& is);
//----------------------------------------------------------------------------------------------------

void TestParseCondition();
//----------------------------------------------------------------------------------------------------

#endif
