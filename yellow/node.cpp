#include "node.h"

/*
    EmptyNode
*/

EmptyNode::EmptyNode()
{
    // empty
};
//----------------------------------------------------------------------------------------------------

bool EmptyNode::Evaluate(const Date& date, const string& event) const noexcept
{
    return true;
};
//----------------------------------------------------------------------------------------------------

/*
    DateComparisonNode
*/

DateComparisonNode::DateComparisonNode(const Comparison& comparison, const Date& date) : comparison(comparison), date(date)
{

};
//----------------------------------------------------------------------------------------------------

bool DateComparisonNode::Evaluate(const Date& date, const string& event) const noexcept
{
    switch (comparison)
    {
        case Comparison::Less:
            return date < this->date;
        case Comparison::LessOrEqual:
            return date <= this->date;
        case Comparison::Greater:
            return date > this->date;
        case Comparison::GreaterOrEqual:
            return date >= this->date;
        case Comparison::Equal:
            return date == this->date;
        case Comparison::NotEqual:
            return date != this->date;
    }

    return false;
};
//----------------------------------------------------------------------------------------------------

/*
    EventComparisonNode
*/

EventComparisonNode::EventComparisonNode(const Comparison& comparison, const string& event) : comparison(comparison), event(event)
{
    // empty
};
//----------------------------------------------------------------------------------------------------

bool EventComparisonNode::Evaluate(const Date& date, const string& event) const noexcept
{
    switch (comparison)
    {
        case Comparison::Less:
            return event < this->event;
        case Comparison::LessOrEqual:
            return event <= this->event;
        case Comparison::Greater:
            return event > this->event;
        case Comparison::GreaterOrEqual:
            return event >= this->event;
        case Comparison::Equal:
            return event == this->event;
        case Comparison::NotEqual:
            return event != this->event;
    }

    return false;
};
//----------------------------------------------------------------------------------------------------

/*
    EventComparisonNode
*/

LogicalOperationNode::LogicalOperationNode(const LogicalOperation& logical_operation, const shared_ptr<Node>& left, const shared_ptr<Node>& right)
    : logical_operation(logical_operation), left(left), right(right)
{
    // empty
};
//----------------------------------------------------------------------------------------------------

bool LogicalOperationNode::Evaluate(const Date& date, const string& event) const noexcept
{
    if (logical_operation == LogicalOperation::And)
    {
        return left->Evaluate(date, event) && right->Evaluate(date, event);
    }
    else
    {
        return left->Evaluate(date, event) || right->Evaluate(date, event);
    }
};
//----------------------------------------------------------------------------------------------------