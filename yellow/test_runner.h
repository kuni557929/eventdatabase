#pragma once

#ifndef HEADERFILE_TEST_RUNNER
#define HEADERFILE_TEST_RUNNER

#include <string>
#include <vector>
#include <iostream>
#include <sstream>
#include <map>

using namespace std; 

typedef void (*lambda)();

class TestRunner
{
public:
    TestRunner() = default;
    ~TestRunner();
    void RunTest(lambda func, const string& name);
private:
    int fail_count = 0;
};
//----------------------------------------------------------------------------------------------------

static ostream& operator << (ostream& os, const vector<string>& vector)
{
    os << "{";
    bool first = true;

    for (const auto & i : vector)
    {
        if (!first)
        {
            os << ", ";
        }

        os << i;
        first = false;
    }

    return os << "}";
};
//----------------------------------------------------------------------------------------------------

template<typename Param1, typename Param2>
static void AssertEqual(const Param1& first, const Param2& second, const std::string& message)
{
    if (first != second)
    {
        std::ostringstream os;
        os << " Assertion failed: " << first << " != " << second << " hint: " << message;
        throw runtime_error(os.str());
    }
};
//----------------------------------------------------------------------------------------------------

static void Assert(bool result, const string& message)
{
    AssertEqual(result, true, message);
};
//----------------------------------------------------------------------------------------------------

#endif