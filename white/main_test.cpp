#include "gtest/gtest.h"
#include "main.cpp"

TEST(DateTest, TestParseDateFirst)
{
    // Test valid date
    Date date("2000-01-01");
    EXPECT_EQ(date.GetYear(), 2000);
    EXPECT_EQ(date.GetMonth(), 1);
    EXPECT_EQ(date.GetDay(), 1);
    EXPECT_EQ(date.ToString(), "2000-01-01");
}

TEST(DateTest, TestParseDateSecond)
{
    // Test valid date
    Date date("1-1-1");
    EXPECT_EQ(date.GetYear(), 1);
    EXPECT_EQ(date.GetMonth(), 1);
    EXPECT_EQ(date.GetDay(), 1);
    EXPECT_EQ(date.ToString(), "0001-01-01");
}

TEST(DateTest, TestParseDateThird)
{
    // Test valid date
    Date date("-1-1-1");
    EXPECT_EQ(date.GetYear(), 1);
    EXPECT_EQ(date.GetMonth(), 1);
    EXPECT_EQ(date.GetDay(), 1);
    EXPECT_EQ(date.ToString(), "0001-01-01");
}

TEST(DateTest, TestParseDateFifth)
{
    // Test valid date
    Date date("1-+1-+1");
    EXPECT_EQ(date.GetYear(), 1);
    EXPECT_EQ(date.GetMonth(), 1);
    EXPECT_EQ(date.GetDay(), 1);
    EXPECT_EQ(date.ToString(), "0001-01-01");
}

TEST(DateTest, TestParseDateSixth)
{
    // Test invalid date
    Date date("2022-02-28");
    EXPECT_EQ(date.GetYear(), 2022);
    EXPECT_EQ(date.GetMonth(), 2);
    EXPECT_EQ(date.GetDay(), 28);
    EXPECT_EQ(date.ToString(), "2022-02-28");
}

TEST(DateTest, TestParseDateSeventh)
{
    // Test valid date
    Date date1("2022-01-01");
    EXPECT_EQ(date1.GetYear(), 2022);
    EXPECT_EQ(date1.GetMonth(), 1);
    EXPECT_EQ(date1.GetDay(), 1);
}

TEST(DateTest, TestParseDateEighth)
{
    // Test valid leap year date
    Date date2("2000-02-29");
    EXPECT_EQ(date2.GetYear(), 2000);
    EXPECT_EQ(date2.GetMonth(), 2);
    EXPECT_EQ(date2.GetDay(), 29);
}

TEST(DateTest, TestParseDateNinth)
{
    // Test valid non-leap year date
    Date date3("1900-02-28");
    EXPECT_EQ(date3.GetYear(), 1900);
    EXPECT_EQ(date3.GetMonth(), 2);
    EXPECT_EQ(date3.GetDay(), 28);
}

TEST(DateTest, TestParseDateTenth)
{
    // Test valid date in the past
    Date date4("2000-01-01");
    EXPECT_EQ(date4.GetYear(), 2000);
    EXPECT_EQ(date4.GetMonth(), 1);
    EXPECT_EQ(date4.GetDay(), 1);
}

TEST(DateTest, TestParseDateEleventh)
{
    // Test valid date in the future
    Date date5("9999-12-31");
    EXPECT_EQ(date5.GetYear(), 9999);
    EXPECT_EQ(date5.GetMonth(), 12);
    EXPECT_EQ(date5.GetDay(), 31);
}

TEST(DateTest, IncorrectDatesFirst)
{
    // Test invalid date format
    EXPECT_THROW(Date date1("2022/01/01"), std::invalid_argument);

    // Test invalid year
    EXPECT_THROW(Date date2("10000-01-01"), std::invalid_argument);

    // Test invalid month
    EXPECT_THROW(Date date3("2022-13-01"), std::invalid_argument);

    // Test invalid day
    EXPECT_THROW(Date date4("2022-02-29"), std::invalid_argument);
}

TEST(DateTest, IncorrectDatesSecond) {
    // Test month out of range
    EXPECT_THROW(Date date2("2022-00-01"), std::invalid_argument);

    // Test day out of range
    EXPECT_THROW(Date date3("2022-02-30"), std::invalid_argument);

    // Test February 30th
    EXPECT_THROW(Date date4("2022-02-30"), std::invalid_argument);

    // Test February 29th on non-leap year
    EXPECT_THROW(Date date5("1900-02-29"), std::invalid_argument);

    // Test empty date
    EXPECT_THROW(Date date6(""), std::invalid_argument);
}

TEST(DateTest, IncorrectDatesThird)
{
    // Test invalid date
    try
    {
        Date("1---1-1");
        FAIL() << "Expected invalid_argument";
    }
    catch (const invalid_argument& e)
    {
        EXPECT_EQ(e.what(), string("Wrong date format: 1---1-1"));
    }
}

TEST(DateTest, IncorrectDatesFourth)
{
    // Test invalid date
    try
    {
        Date("1111-32-11");
        FAIL() << "Expected invalid_argument";
    }
    catch (const invalid_argument& e)
    {
        EXPECT_EQ(e.what(), string("Month value is invalid: 32"));
    }
}

TEST(DateTest, IncorrectDatesFifth)
{
    // Test invalid date
    try
    {
        Date("1111-11-32");
        FAIL() << "Expected invalid_argument";
    }
    catch (const invalid_argument& e)
    {
        EXPECT_EQ(e.what(), string("Day value is invalid: 32"));
    }
}

TEST(DateTest, TestComparisonOperators)
{
    // Test == operator
    Date date1("2000-01-01");
    Date date2("2000-01-01");
    EXPECT_EQ(date1, date2);

    // Test < operator
    date2 = Date("2000-02-01");
    EXPECT_LT(date1, date2);
}

TEST(DataBaseRepositoryTest, TestAddEvent)
{
    DataBaseRepository repository;
    Date date("2000-01-01");

    // Test adding first event
    repository.AddEvent(date, "Event 1");
    auto events = repository.GetSortedEventsByDate(date);
    EXPECT_EQ(events.size(), 1);
    EXPECT_EQ(events[0], "Event 1");

    // Test adding second event
    repository.AddEvent(date, "Event 2");
    events = repository.GetSortedEventsByDate(date);
    EXPECT_EQ(events.size(), 2);
    EXPECT_EQ(events[1], "Event 2");
}

TEST(DataBaseRepositoryTest, TestDeleteEvent)
{
    DataBaseRepository repository;
    Date date("2000-01-01");

    // Test deleting non-existing event
    repository.AddEvent(date, "Event 1");
    EXPECT_FALSE(repository.DeleteEventByDate(date, "Event 2"));

    // Test deleting existing event
    EXPECT_TRUE(repository.DeleteEventByDate(date, "Event 1"));
    auto events = repository.GetSortedEventsByDate(date);
    EXPECT_EQ(events.size(), 0);
}

TEST(DataBaseRepositoryTest, TestDeleteAllEvents)
{
    DataBaseRepository repository;
    Date date("2000-01-01");

    // Test deleting events on non-existing date
    EXPECT_EQ(repository.DeleteAllEventsByDate(date), 0);

    // Test deleting events on existing date
    repository.AddEvent(date, "Event 1");
    repository.AddEvent(date, "Event 2");
    EXPECT_EQ(repository.DeleteAllEventsByDate(date), 2);
}

TEST(DataBaseRepositoryTest, TestAddAndFindEvent)
{
    DataBaseRepository repository;
    Date date1("2000-01-02");
    Date date2("2000-02-03");

    // Test adding events
    repository.AddEvent(date1, "event1");
    repository.AddEvent(date2, "event2");
    // Test finding events
    auto events = repository.GetSortedEventsByDate(date1);
    EXPECT_EQ(events.size(), 1);
    EXPECT_EQ(events[0], "event1");
}

TEST(DataBaseRepositoryTest, TestDeleteEventSecond)
{
    DataBaseRepository repository;
    Date date1("2000-01-02");
    Date date2("2000-02-03");

    // Test deleting non-existing date
    EXPECT_EQ(repository.DeleteAllEventsByDate(date1), 0);

    // Test adding and deleting events
    repository.AddEvent(date1, "event1");
    repository.AddEvent(date2, "event2");
    EXPECT_EQ(repository.DeleteAllEventsByDate(date1), 1);

    // Test deleting non-existing event
    EXPECT_FALSE(repository.DeleteEventByDate(date2, "event1"));

    // Test deleting existing event
    EXPECT_TRUE(repository.DeleteEventByDate(date2, "event2"));
    EXPECT_EQ(repository.GetSortedEventsByDate(date2).size(), 0);
}

TEST(ApplicationSplitCommandTest, SplitCommandFirst)
{
    Application application;
    vector<string> expected_parts = {"Add", "1-1-2", "event1"};

    // Test split exists command
    string command = "Add 1-1-2 event1";
    vector<string> result = application.SplitCommand(command);
    EXPECT_EQ(result.size(), expected_parts.size());
    EXPECT_EQ(result[0], expected_parts[0]);
    EXPECT_EQ(result[1], expected_parts[1]);
    EXPECT_EQ(result[2], expected_parts[2]);
}

TEST(ApplicationSplitCommandTest, SplitCommandSecond)
{
    Application application;
    vector<string> expected_parts = {"Add", "1-1-2", "event1"};

    // Test split command with spaces
    string command = "    Add       1-1-2            event1     ";
    vector<string> result = application.SplitCommand(command);
    EXPECT_EQ(result.size(), expected_parts.size());
    EXPECT_EQ(result[0], expected_parts[0]);
    EXPECT_EQ(result[1], expected_parts[1]);
    EXPECT_EQ(result[2], expected_parts[2]);
}

TEST(ApplicationSplitCommandTest, SplitCommandThird)
{
    Application application;
    vector<string> expected_parts = {"Find", "1-1-2"};

    // Test split command with two parameters
    string command = "Find 1-1-2";
    vector<string> result = application.SplitCommand(command);
    EXPECT_EQ(result.size(), expected_parts.size());
    EXPECT_EQ(result[0], expected_parts[0]);
    EXPECT_EQ(result[1], expected_parts[1]);
}

TEST(ApplicationSplitCommandTest, SplitCommandFourth)
{
    Application application;
    vector<string> expected_parts = {"Find"};

    // Test split command with one parameter
    string command = "Find";
    vector<string> result = application.SplitCommand(command);
    EXPECT_EQ(result.size(), expected_parts.size());
    EXPECT_EQ(result[0], expected_parts[0]);
}

TEST(ApplicationSplitCommandTest, SplitCommandFifth)
{
    Application application;
    vector<string> expected_parts;

    // Test split empty command
    string command;
    vector<string> result = application.SplitCommand(command);
    EXPECT_EQ(result.size(), expected_parts.size());
}

TEST(ApplicationSplitCommandTest, SplitCommandSixth)
{
    Application application;
    vector<string> expected_parts;

    // Test split command with only spaces
    string command = "                                  ";
    vector<string> result = application.SplitCommand(command);
    EXPECT_EQ(result.size(), expected_parts.size());
}

TEST(DataBaseCommandContainerTest, GetProperlyCommandByName)
{
    DataBaseCommandContainer command_container;

    // Testing getting the command properly
    string command = "ADD";
    EXPECT_TRUE(dynamic_cast<AddEventDataBaseCommand*>(command_container.GetCommand(command)) != nullptr);

    command = "PriNt";
    EXPECT_TRUE(dynamic_cast<PrintAllEventsDataBaseCommand*>(command_container.GetCommand(command)) != nullptr);

    command = "del";
    EXPECT_TRUE(dynamic_cast<DeleteEventByDateDataBaseCommand*>(command_container.GetCommand(command)) != nullptr);

    command = "find";
    EXPECT_TRUE(dynamic_cast<FindEventsByDateDataBaseCommand*>(command_container.GetCommand(command)) != nullptr);
}

TEST(DataBaseCommandContainerTest, GetNotProperlyCommandByName)
{
    DataBaseCommandContainer command_container;

    // Testing for receiving an unknown command
    string command = "UNKNOWN";
    try
    {
        command_container.GetCommand(command);
    }
    catch (const invalid_argument& ex)
    {
        EXPECT_EQ(ex.what(), "Unknown command: " + command);
    }
}

TEST(DataBaseRepositoryTests, AddEvent)
{
    DataBaseRepository repository;
    Date date1("2022-01-01");
    Date date2("2022-01-02");

    repository.AddEvent(date1, "Event 1");
    repository.AddEvent(date1, "Event 2");
    repository.AddEvent(date2, "Event 3");

    auto storage = repository.GetStorageSortedByKeyAndValue();
    auto events1 = repository.GetSortedEventsByDate(date1);
    auto events2 = repository.GetSortedEventsByDate(date2);

    // Check that events were added to the correct date
    EXPECT_EQ(storage.size(), 2);
    EXPECT_EQ(events1.size(), 2);
    EXPECT_EQ(events2.size(), 1);

    // Check that events are sorted
    EXPECT_EQ(events1[0], "Event 1");
    EXPECT_EQ(events1[1], "Event 2");
    EXPECT_EQ(events2[0], "Event 3");
}

TEST(DataBaseRepositoryTests, GetStorageSortedByKeyAndValue)
{
    DataBaseRepository repository;
    Date date1("2022-01-01");
    Date date2("2022-01-02");
    Date date3("2022-02-01");

    repository.AddEvent(date2, "Event 1");
    repository.AddEvent(date3, "Event 2");
    repository.AddEvent(date1, "Event 3");

    auto storage = repository.GetStorageSortedByKeyAndValue();

    // Check that storage is sorted by key
    EXPECT_EQ(storage.begin()->first, date1);
    EXPECT_EQ(next(storage.begin())->first, date2);
    EXPECT_EQ(next(storage.begin(), 2)->first, date3);

    // Check that events within each date are sorted
    auto events1 = repository.GetSortedEventsByDate(date1);
    auto events2 = repository.GetSortedEventsByDate(date2);
    auto events3 = repository.GetSortedEventsByDate(date3);

    EXPECT_EQ(events1[0], "Event 3");
    EXPECT_EQ(events2[0], "Event 1");
    EXPECT_EQ(events3[0], "Event 2");
}

TEST(DataBaseRepositoryTests, GetSortedEventsByDate)
{
    DataBaseRepository repository;
    Date date("2022-01-01");

    repository.AddEvent(date, "Event 2");
    repository.AddEvent(date, "Event 1");
    repository.AddEvent(date, "Event 3");

    auto events = repository.GetSortedEventsByDate(date);

    // Check that events are sorted
    EXPECT_EQ(events[0], "Event 1");
    EXPECT_EQ(events[1], "Event 2");
    EXPECT_EQ(events[2], "Event 3");
}

TEST(DataBaseRepositoryTests, IsDateExists)
{
    DataBaseRepository repository;
    Date date1("2022-01-01");
    Date date2("2022-01-02");

    repository.AddEvent(date1, "Event 1");

    EXPECT_TRUE(repository.IsDateExists(date1));
    EXPECT_FALSE(repository.IsDateExists(date2));
}

TEST(DataBaseRepositoryTests, IsEventExistsForDate)
{
    DataBaseRepository repository;
    Date date("2022-01-01");

    repository.AddEvent(date, "Event 1");

    EXPECT_TRUE(repository.IsEventExistsForDate(date, "Event 1"));
    EXPECT_FALSE(repository.IsEventExistsForDate(date, "Event 2"));
    EXPECT_FALSE(repository.IsEventExistsForDate(Date("2022-01-02"), "Event 1"));
}

TEST(DataBaseRepositoryTests, DeleteEventByDate)
{
    DataBaseRepository repository;
    Date date("2022-01-01");

    repository.AddEvent(date, "Event 1");
    repository.AddEvent(date, "Event 2");

    EXPECT_TRUE(repository.DeleteEventByDate(date, "Event 1"));
    EXPECT_FALSE(repository.IsEventExistsForDate(date, "Event 1"));
    EXPECT_FALSE(repository.DeleteEventByDate(date, "Event 1"));
    EXPECT_FALSE(repository.DeleteEventByDate(Date("2022-01-02"), "Event 1"));
}

TEST(DataBaseRepositoryTests, DeleteAllEventsByDate)
{
    DataBaseRepository repository;
    Date date1("2022-01-01");
    Date date2("2022-01-02");

    repository.AddEvent(date1, "Event 1");
    repository.AddEvent(date1, "Event 2");
    repository.AddEvent(date2, "Event 3");

    EXPECT_EQ(repository.DeleteAllEventsByDate(date1), 2);
    EXPECT_FALSE(repository.IsDateExists(date1));
    EXPECT_EQ(repository.DeleteAllEventsByDate(date1), 0);
}

TEST(DataBaseRepositoryTests, DeleteAllEventsByDate_NoEvents)
{
    DataBaseRepository repository;

    EXPECT_EQ(repository.DeleteAllEventsByDate(Date("2022-01-01")), 0);
}

TEST(DataBaseRepositoryTests, IsEventExistsForDate_NoEvents)
{
    DataBaseRepository repository;

    EXPECT_FALSE(repository.IsEventExistsForDate(Date("2022-01-01"), "Event 1"));
}

TEST(DataBaseRepositoryTests, DeleteEventByDate_NoEvents)
{
    DataBaseRepository repository;

    EXPECT_FALSE(repository.DeleteEventByDate(Date("2022-01-01"), "Event 1"));
}