#include <iostream>
#include <map>
#include <vector>
#include <algorithm>
#include <regex>
#include <iomanip>

using namespace std;

class Date
{
public:
    explicit Date(const string& date)
    {
        regex date_format(R"(^-?\+?\d{1,4}\-\+?\d{1,2}\-\+?\d{1,2}$)");

        if (!regex_match(date, date_format))
        {
            throw invalid_argument("Wrong date format: " + date);
        }

        vector<int> parts;

        regex delimiter("-");
        sregex_token_iterator end;

        for (sregex_token_iterator it(date.begin(), date.end(), delimiter, -1); it != end; ++it)
        {
            string part = *it;

            if (!part.empty())
            {
                parts.push_back(abs(stoi(part)));
            }
        }

        _year = parts[0];
        _month = parts[1];
        _day = parts[2];

        const string& result = this->Validate();

        if (!result.empty())
        {
            throw invalid_argument(result);
        }
    };

    bool operator == (const Date& date) const
    {
        return _year  == date.GetYear()  &&
               _month == date.GetMonth() &&
               _day   == date.GetDay();
    };

    bool operator < (const Date& date) const
    {
        return (_year < date.GetYear()) ||
               (_year == date.GetYear() && _month < date.GetMonth()) ||
               (_year == date.GetYear() && _month == date.GetMonth() && _day < date.GetDay());
    };

    int GetYear() const
    {
        return _year;
    };

    int GetMonth() const
    {
      return _month;
    };

    int GetDay() const
    {
        return _day;
    };

    string ToString() const
    {
        std::ostringstream dateStream;
        dateStream << std::setfill('0') << std::setw(4) << this->GetYear()  << '-';
        dateStream << std::setfill('0') << std::setw(2) << this->GetMonth() << '-';
        dateStream << std::setfill('0') << std::setw(2) << this->GetDay();
        return dateStream.str();
    };
private:
    int _year;
    int _month;
    int _day;
private:
    string Validate() const
    {
        if (_month < 1 || _month > 12)
        {
            return "Month value is invalid: " + to_string(_month);
        }

        if (_day < 1 || _day > DaysInMonth(_month, _year))
        {
            return "Day value is invalid: " + to_string(_day);
        }

        return "";
    };

    int DaysInMonth(int month, int year) const
    {
        switch (month)
        {
            case 2:
                return IsLeapYear(year) ? 29 : 28;
            case 4:
            case 6:
            case 9:
            case 11:
                return 30;
            default:
                return 31;
        }
    };

    bool IsLeapYear(int year) const
    {
        return year % 4 == 0 && (year % 100 != 0 || year % 400 == 0);
    };
};

class DataBaseRepository
{
public:
    DataBaseRepository() : _storage(new map<Date, vector<string>*>())
    {

    };

    virtual ~DataBaseRepository()
    {
        for (const auto& item : *_storage)
        {
            delete item.second;
        }

        delete _storage;
    };
public:
    const map<Date, vector<string>*>& GetStorageSortedByKeyAndValue() const
    {
        return *_storage;
    };

    const vector<string>& GetSortedEventsByDate(const Date& date) const
    {
        if (!IsDateExists(date))
        {
            static const vector<string> empty;
            return empty;
        }

        return *_storage->at(date);
    };

    bool IsDateExists(const Date& date) const
    {
        return !(_storage->find(date) == _storage->end());
    };

    bool IsEventExistsForDate(const Date& date, const string& event) const
    {
        try
        {
            const vector<string>* events = _storage->at(date);
            return find(events->begin(), events->end(), event) != events->end();
        }
        catch (...)
        {
            return false;
        }
    };

    void AddEvent(const Date& date, const string& event)
    {
        if (this->IsDateExists(date))
        {
            _storage->at(date)->push_back(event);
        }
        else
        {
            auto* events = new vector<string>();
            events->push_back(event);

            _storage->insert(make_pair(date, events));
        }

        vector<string>* events = _storage->at(date);
        sort(events->begin(), events->end());
    };

    bool DeleteEventByDate(const Date& date, const string& event)
    {
        if (IsDateExists(date) && IsEventExistsForDate(date, event))
        {
            vector<string>* events = _storage->at(date);
            events->erase(remove(events->begin(), events->end(), event), events->end());
            return true;
        }

        return false;
    };

    uint32_t DeleteAllEventsByDate(const Date& date)
    {
        if (IsDateExists(date))
        {
            vector<string>* events = _storage->at(date);
            uint32_t deleted_events = events->size();
            delete events;
            _storage->erase(_storage->find(date));

            return deleted_events;
        }

        return 0;
    };
private:
    map<Date, vector<string>*>* const _storage;
};

class DataBaseCommand
{
public:
    explicit DataBaseCommand(DataBaseRepository* repository): _repository(repository)
    {

    };

    virtual ~DataBaseCommand()
    {
        delete _repository;
    };

    virtual void Execute(const vector<string>& parameters) const = 0;
protected:
    DataBaseRepository* const _repository;
};

class AddEventDataBaseCommand : public DataBaseCommand
{
public:
    explicit AddEventDataBaseCommand(DataBaseRepository* repository): DataBaseCommand(repository)
    {

    };
public:
    void Execute(const vector<string>& parameters) const override
    {
        if (parameters.size() != 3)
        {
            throw invalid_argument("Incorrect number of parameters");
        }

        const Date date(parameters[1]);
        const string& event = parameters[2];

        if (_repository->IsEventExistsForDate(date, event))
        {
            return;
        }

        _repository->AddEvent(date, event);
    };
};

class DeleteEventByDateDataBaseCommand : public DataBaseCommand
{
public:
    explicit DeleteEventByDateDataBaseCommand(DataBaseRepository* repository): DataBaseCommand(repository)
    {

    };
public:
    void Execute(const vector<string>& parameters) const override
    {
        if (parameters.size() > 3)
        {
            throw invalid_argument("Incorrect number of parameters");
        }

        const Date date(parameters[1]);

        if (parameters.size() == 2)
        {
            cout << "Deleted " + to_string(_repository->DeleteAllEventsByDate(date)) + " events" << endl;
        }
        else
        {
            const string& event = parameters[2];

            if (_repository->DeleteEventByDate(date, event))
            {
                cout << "Deleted successfully" << endl;
            }
            else
            {
                cout << "Event not found" << endl;
            }
        }
    };
};

class PrintAllEventsDataBaseCommand : public DataBaseCommand
{
public:
    explicit PrintAllEventsDataBaseCommand(DataBaseRepository* repository): DataBaseCommand(repository)
    {

    };
public:
    void Execute(const vector<string>& parameters) const override
    {
        if (parameters.size() != 1)
        {
            throw invalid_argument("Incorrect number of parameters");
        }

        const map<Date, vector<string>*>& storage = _repository->GetStorageSortedByKeyAndValue();

        for (const auto& pair : storage)
        {
            const Date& date = pair.first;
            const vector<string>& events = *pair.second;

            for (const auto& event : events)
            {
                cout << date.ToString() << " " << event << endl;
            }
        }
    };
};

class FindEventsByDateDataBaseCommand : public DataBaseCommand
{
public:
    explicit FindEventsByDateDataBaseCommand(DataBaseRepository* repository): DataBaseCommand(repository)
    {

    };
public:
    void Execute(const vector<string>& parameters) const override
    {
        if (parameters.size() != 2)
        {
            throw invalid_argument("Incorrect number of parameters");
        }

        const Date date(parameters[1]);

        if (_repository->IsDateExists(date))
        {
            const vector<string>& events = _repository->GetSortedEventsByDate(date);

            for (const auto& item : events)
            {
                cout << item << endl;
            }
        }
    };
};

class DataBaseCommandContainer
{
public:
    explicit DataBaseCommandContainer() : _command_container(new map<string, DataBaseCommand*>())
    {
        auto* repository = new DataBaseRepository();
        _command_container->insert(make_pair("add", new AddEventDataBaseCommand(repository)));
        _command_container->insert(make_pair("print", new PrintAllEventsDataBaseCommand(repository)));
        _command_container->insert(make_pair("del", new DeleteEventByDateDataBaseCommand(repository)));
        _command_container->insert(make_pair("find", new FindEventsByDateDataBaseCommand(repository)));
    };

    virtual ~DataBaseCommandContainer()
    {
        delete _command_container;
    };
public:
    DataBaseCommand* GetCommand(string& command) const
    {
        try
        {
            transform(command.begin(), command.end(), command.begin(), ::tolower);
            return _command_container->at(command);
        }
        catch (...)
        {
            throw invalid_argument("Unknown command: " + command);
        }
    };
private:
    map<string, DataBaseCommand*>* const _command_container;
};

class Application
{
public:
    Application() : _container(new DataBaseCommandContainer())
    {

    };

    virtual ~Application()
    {
        delete _container;
    };
public:
    int Start() const
    {
        try
        {
            string line;

            while (getline(cin, line, '\n'))
            {
                vector<string> parts = this->SplitCommand(line);

                if (!parts.empty())
                {
                    _container->GetCommand(parts.at(0))->Execute(parts);
                }
            }
        }
        catch (const exception& ex)
        {
            cout << ex.what() << endl;
        }

        return EXIT_SUCCESS;
    };

    vector<string> SplitCommand(string& command) const
    {
        command = regex_replace(command, regex("^ +| +$|( ) +"), "$1");

        vector<string> parts;

        if (command.empty())
        {
            return parts;
        }
        else
        {
            regex delimiter("\\s+");
            sregex_token_iterator end;

            for (sregex_token_iterator it(command.begin(), command.end(), delimiter, -1); it != end; ++it)
            {
                parts.push_back(*it);
            }

            return parts;
        }
    };
private:
    const DataBaseCommandContainer* _container;
};

int main()
{
    Application app;
    return app.Start();
};
